=======================================
== Advanced Mechanics (updates) $AM3 ==
=======================================
[h1]Tunneling[/h1]

When kiting a large swarm of skulls, you can reverse directions and cut low to "tunnel" through them.

[h1]Centipedes (2021 note)[/h1]

In February 2021, the game received a QoL update for its 5th anniversary. Among other things, centipede AI was changed
to be more gigapede-like. This means that they won't immediately burrow when you get under them. This means that the
cheese strat demonstrated in "Centipedes the right way" is not as viable as it used to be. This is all well and good,
though, because now it means you can use centipedes to practice for gigapedes. I believe new players will have an easier
time getting used to gigapedes by the time they get to them, thanks to this update. 

You can watch my pink run guide to see an example of a good way to handle centipedes.

[previewyoutube=CjkYVlzLsuQ;full][/previewyoutube]

I also made a centipede guide in 2020, although it's slightly outdated because of the new AI. The guide still works
because I showcased how to actually manipulate their AI rather than just cheese them.

[previewyoutube=LgGrHmgGWYw;full][/previewyoutube]

[h1]Reversing Directions[/h1]

Simply reversing directions can be used in a lot of situations. Because skulls follow (for the most part) a predictable
boid pathfinding AI, you can manipulate them into going into a circle (kiting), and then abruptly reverse your
direction. This will force them to run into you at a "straight" line, and it can be exploited. Another way this can be
exploited is in a scenario where you are in a triangle with two far away squids. You can W+D strafe towards the one on
the right with a healthy stream to cut down most of its swarm, and then W+A kite towards the one on the left to cut them
down. In the act of strafing towards the right squid, you'd led the left swarm in a semi-circular path towards you, so
by abruptly switching your direction, you line right up with them. This is very important in the end loop.

Also important to note is that skulls have a tendency to hover near to the ground, so gently sweeping left to right and
low-to-the-ground is a great way to tunnel and cull skulls.

[h1]Video Demonstrations[/h1]

I recorded a couple farming runs to try to showcase these techniques. It's subtle, but powerful. The video on
"cut-through" was [i]sort of[/i] trying to get at this, but I didn't fully understand it at the time, and that video
specifically was more trying to show how you can break your kite to either bait Skull IIs towards you or avoid them when
they get close.

[previewyoutube=x63aQLR_nbI;full][/previewyoutube]
