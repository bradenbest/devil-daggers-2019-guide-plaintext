=========================================
== Advanced Mechanics (continued) $AM2 ==
=========================================
[h1]Scraping Gigapedes - Nullifier's strat[/h1]

Nullifier's strategy for handling the Gigapedes is similar to Bintr's.

Doing this involves...
* getting on the opposite side of the arena from the smoke clouds
* choosing a good target
* prefiring said target while it's coming for you
* waiting until said target is over your head
* charging forward while maintaining aim on the same spot on the target
* going around the target and getting under its arch
* stopping and tilting your aim up to speed up the scrape for about 1-2s
* backing out while reversing the aim tilt to maintain aim on the same spot

Executing all of these steps correctly will easily result in 45-50 of the gems coming off.

It's hard to explain, so here's two videos

[previewyoutube=JnGjoMPuWdM;full][/previewyoutube]
[previewyoutube=IfDJ3Xg-ZY4;full][/previewyoutube]

[h1]Farming[/h1]

Farming is when you leave Squids alive for extra gems. I consider there to be four types of farms:

1. Easy farm - killing almost everything and only collecting enough to get exactly 70 gems by 134s.
2. 80 farm - leaving all SQUID I alive until 80s. Whether you kill SQUID II  or leave them alive for extra gushes is up
to you
3. 130 farm - leaving all Squids alive until 130s.
4. Chaos farm - leaving all Squids alive until you get level 4 at around 240s. You can get away with killing a couple
SQUID II at 100s and most top players do this when chaos farming.

80 farm is the farm demonstrated in the walkthrough section. It can get annoying, which is why I developed the Easy
farm. There are many ways to do the easy farm, but my preferred way is to kill the first 3 Squids, let everything after
(except 94s Squids) gush a second time, including the 109s Squid. Doing this correctly will result in a clean, easy farm
and exactly 70 gems when you get to the 134s wave.

Here's a video

[previewyoutube=1LCIS7roS14;full][/previewyoutube]

[h1]Stretching[/h1]

Stretching is what you do when you've completely lost control of the arena -- trying to survive for as long as possible.
A good stretch requires proficiency with movement and a good understanding of enemy movement, and what you can and can't
get away with. By the time you get your first 500, you'll know very well what a stretch is. Trust me.

[h1]Eclipsing[/h1]

Normally, when two Squids spawn on opposite sides of the arena, the strategy is to focus one down, turn around and focus
the other down. Eclipsing (and I just coined this now) is when you get close to one of the Squids so that it gushes past
you, in such a way that you're facing both swarms at once. Doing this lets you kill both swarms at once. This strategy
is most effective on levels 3 and 4.

[h1]Prefiring[/h1]

This is closely related to the stream shot technique. Prefiring can refer to a lot of things, but generally, it refers
to firing at an enemy early to get the jump on them. You can prefire Squids, Spiders, Centipedes and Gigapedes.
