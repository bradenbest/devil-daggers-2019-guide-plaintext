==================================
== 0 - 39 (Early Game 1) $SEC01 ==
==================================
[h1]Enemies[/h1]
[list]
[*][url=https://devildaggers.info/Wiki/Enemies#SquidI]SQUID I[/url]
[*][url=https://devildaggers.info/Wiki/Enemies#SkullI]SKULL I[/url]
[*][url=https://devildaggers.info/Wiki/Enemies#SkullII]SKULL II[/url]
[/list]

From 0 - 39 seconds, you will be bombarded by 4 SQUID I. These guys have a single red gem, which is their only weak
point. You can shoot the red gem, or the area immediately under the red gem. They gush every 20 seconds after spawning
and float towards the center of the arena. They gush 10 SKULL I and 1 SKULL II.

[h1]Strategy (no farm)[/h1]

Aim your stream directly above the top of the SQUID I, where the tentacles start. Once the SKULL II emerges, your stream
will catch it and it will start flashing, indicating it's stunned. Do a shotgun blast, that should kill it and most of
the swarm. You'll want to shake your stream in a light circle to clean up the rest of the swarm. Moving backwards in
small stutters while doing this helps. Try to learn how to do this quickly.

Now kill the SQUID I, and repeat this for the next 3. Be sure to have everything dead and all the gems collected before
39s, which is when the SPIDER I spawns.

[previewyoutube=coXRYKVzNmM;full][/previewyoutube]

[h1]Strategy (farm)[/h1]

You should only be farming if you're stuck at the Gigapedes, as it helps you get the level 4 hand a bit earlier.
Otherwise, there's no real point. Although if you want to do it for practice, to get better at handling and killing
skulls, go for it. I don't recommend attempting this unless you can easily and consistently kill everything in this wave
before 35s.

You're basically doing the same thing as above, except you aren't killing those SQUID I.

You'll want to keep a mental map of where each SQUID I is. After Squid #3 gushes, stay close to it (or remember where it
is) and kill the spawn from Squid #1. Then stay where you are and stream Squid #4's spawn to death. Try to do it
quickly, because at 37s, Squid #2 will gush its second swarm. It's important to prioritize the SKULL I first and then
take down the SKULL II, and do this quickly. And it's also important to have a system for figuring out which SQUID I is
Squid #2, so you can take it out quickly. Staying close to Squid #3 helps eliminate one SQUID I from the pool so you can
just get up to the one in the middle. You can also kill Squid #3 if it's giving you too much trouble when the SPIDER I
spawns. Or you can kill Squid #2 if you're having a hard time killing its spawn in time.

[previewyoutube=sLgr6ACB8Iw;full][/previewyoutube]
