#!/bin/bash

outfile=guide.txt

echo -ne "" > $outfile

append(){
    cat $1 >> $outfile
    echo -ne "\n\n\n" >> $outfile
}

for file in src/* ; do
    append $file
done
